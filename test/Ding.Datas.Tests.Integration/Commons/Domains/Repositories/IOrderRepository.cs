﻿using Ding.Datas.Tests.Commons.Domains.Models;
using Ding.Domains.Repositories;

namespace Ding.Datas.Tests.Commons.Domains.Repositories {
    /// <summary>
    /// 订单仓储
    /// </summary>
    public interface IOrderRepository : IRepository<Order> {
    }
}
