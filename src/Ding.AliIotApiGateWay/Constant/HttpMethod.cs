﻿namespace Ding.Iot.Constant
{
    public class HttpMethod
    {
        //GET
        public const string GET = "GET";

        //POST
        public const string POST = "POST";

        //PUT
        public const string PUT = "PUT";

        //DELETE
        public const string DELETE = "DELETE";

        //DELETE
        public const string HEAD = "HEAD";
    }
}
