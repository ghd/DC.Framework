﻿namespace Ding.Iot.Constant
{
    public class HttpSchema
    {
        //HTTP
        public const string HTTP = "http://";

        //HTTPS
        public const string HTTPS = "https://";
    }
}
