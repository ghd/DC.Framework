﻿namespace Ding.Iot.Enums
{
    public enum Method
    {
        GET, POST_FORM, POST_STRING, POST_BYTES, PUT_STRING, PUT_BYTES, DELETE
    }
}
