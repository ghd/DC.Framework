﻿using LiteDB;

namespace Ding.LiteDB
{
    public interface ILiteDbContext
    {
        LiteDatabase Database { get; }
    }
}
