﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ding.LiteDB
{
    /// <summary>
    /// LiteDB扩展
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// 注册短信操作
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="setupAction">配置操作</param>
        public static void AddLiteDB(this IServiceCollection services, Action<LiteDbOptions> setupAction)
        {
            services.Configure(setupAction);
            services.AddSingleton<ILiteDbContext, LiteDbContext>();
        }
    }
}
