﻿namespace Ding.Payment.UnionPay
{
    /// <summary>
    /// UnionPay 通知。
    /// </summary>
    public abstract class UnionPayNotify : UnionPayObject
    {
    }
}
