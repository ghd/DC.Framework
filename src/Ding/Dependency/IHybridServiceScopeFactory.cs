﻿using Microsoft.Extensions.DependencyInjection;

namespace Ding.Dependency
{
    /// <summary>
    /// 混合服务作用域工厂
    /// </summary>
    public interface IHybridServiceScopeFactory : IServiceScopeFactory
    {
    }
}
