﻿using Ding.Caches;
using Ding.Security;
using Ding.Timing;
using System;

namespace Ding.Helpers
{
    /// <summary>
    /// 接口安全检验
    /// </summary>
    public class CheckSignature
    {
        public static readonly string Token = "hlktech_test";

        /// <summary>
        /// 检查通讯密钥
        /// </summary>
        /// <param name="signature">通信加密签名</param>
        /// <param name="timestamp">时间戳</param>
        /// <param name="nonce">随机字符串</param>
        /// <param name="token">通讯密钥</param>
        /// <param name="retusnsignature">检验值</param>
        /// <returns></returns>
        public static bool Check(string signature, long timestamp, string nonce, string token, out string retusnsignature)
        {
            retusnsignature = "";

            var time = UnixTime.ToDateTime(timestamp);
            if (time.AddSeconds(30) < DateTime.Now) return false;

            var _cache = Ioc.Create<ICache>();
            if (_cache.Exists(signature))
            {
                return false;
            }
            else
            {
                _cache.Add(signature, "", TimeSpan.FromSeconds(30));
            }

            token = (token ?? Token);
            string[] array = new string[] { timestamp.ToString(), nonce, token };
            Array.Sort<string>(array);  //升序
            string text = string.Join("", array); //在指定 String 数组的每个元素之间串联指定的分隔符 String，从而产生单个串联的字符串
            //text = DESEncrypt.Encrypt(text, 0);
            text = EncryptHelper.GetSha1(text);
            retusnsignature = text.ToLower();
            return signature == retusnsignature;
        }

        public static string Create(long timestamp, string nonce, string token)
        {
            token = (token ?? Token);
            string[] array = new string[] { timestamp.ToString(), nonce, token };
            Array.Sort<string>(array);  //升序
            string text = string.Join("", array); //在指定 String 数组的每个元素之间串联指定的分隔符 String，从而产生单个串联的字符串
            //text = DESEncrypt.Encrypt(text, 0);
            text = EncryptHelper.GetSha1(text);
            return text.ToLower();
        }

    }
}
