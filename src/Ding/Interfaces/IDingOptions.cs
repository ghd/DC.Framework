﻿namespace Ding.Interfaces
{
    /// <summary>
    ///     Implement this interface to become one of configuration options in Elect Ecosystem 
    /// </summary>
    public interface IDingOptions
    {
    }
}
