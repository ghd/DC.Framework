﻿namespace Ding.Biz.OAuthLogin.GitHub.Configs
{
    /// <summary>
    /// 配置
    /// 
    /// 步骤：authorize => access_token => user
    /// </summary>
    public class GitHubConfig : ConfigBase
    {
        #region API请求接口

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize = "https://github.com/login/oauth/authorize";

        /// <summary>
        /// POST
        /// </summary>
        public static string API_AccessToken = "https://github.com/login/oauth/access_token";

        /// <summary>
        /// GET
        /// </summary>
        public static string API_User = "https://api.github.com/user";

        #endregion

        /// <summary>
        /// github 申请的应用名称
        /// </summary>
        public string ApplicationName { get; set; } =  "";
    }
}
