﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.AliPay.Configs
{
    /// <summary>
    /// AliPay配置提供器
    /// </summary>
    public interface IAliPayConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<AliPayConfig> GetConfigAsync();
    }
}
