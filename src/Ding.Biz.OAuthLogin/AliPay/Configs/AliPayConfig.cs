﻿namespace Ding.Biz.OAuthLogin.AliPay.Configs
{
    /// <summary>
    /// 配置
    /// 
    /// 步骤：authorize => access_token
    /// </summary>
    public class AliPayConfig : ConfigBase
    {
        #region API请求接口

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm";

        /// <summary>
        /// 网关
        /// </summary>
        public static string API_Gateway = "https://openapi.alipay.com/gateway.do";

        #endregion
    }
}
