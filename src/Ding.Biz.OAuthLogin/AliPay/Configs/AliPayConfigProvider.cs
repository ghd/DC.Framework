﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.AliPay.Configs
{
    /// <summary>
    /// GitHub配置提供器
    /// </summary>
    public class AliPayConfigProvider : IAliPayConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly AliPayConfig _config;

        /// <summary>
        /// 初始化GitHub配置提供器
        /// </summary>
        /// <param name="config"></param>
        public AliPayConfigProvider(AliPayConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<AliPayConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
