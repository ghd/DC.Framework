﻿namespace Ding.Biz.OAuthLogin.TaoBao.Configs
{
    /// <summary>
    /// 配置 
    /// 
    /// 步骤：authorize => access_token
    /// </summary>
    public class TaoBaoConfig : ConfigBase
    {
        #region API请求接口

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize = "https://oauth.taobao.com/authorize";

        /// <summary>
        /// POST
        /// </summary>
        public static string API_AccessToken = "https://oauth.taobao.com/token";

        #endregion
    }
}
