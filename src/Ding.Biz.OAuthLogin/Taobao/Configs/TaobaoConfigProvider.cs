﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.TaoBao.Configs
{
    /// <summary>
    /// QQ配置提供器
    /// </summary>
    public class TaoBaoConfigProvider : ITaoBaoConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly TaoBaoConfig _config;

        /// <summary>
        /// 初始化QQ配置提供器
        /// </summary>
        /// <param name="config"></param>
        public TaoBaoConfigProvider(TaoBaoConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<TaoBaoConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
