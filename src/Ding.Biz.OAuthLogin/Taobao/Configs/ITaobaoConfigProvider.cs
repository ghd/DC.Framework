﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.TaoBao.Configs
{
    /// <summary>
    /// Taobao配置提供器
    /// </summary>
    public interface ITaoBaoConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<TaoBaoConfig> GetConfigAsync();
    }
}
