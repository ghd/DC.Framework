﻿using Ding.Biz.OAuthLogin.Gitee.Configs;
using Ding.Helpers;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// access token 请求参数
    /// </summary>
    public class Gitee_AccessToken_RequestEntity
    {
        /// <summary>
        /// DingTalk登录配置
        /// </summary>
        protected static readonly GiteeConfig GiteeConfig;

        /// <summary>
        /// 初始化一个<see cref="Gitee_AccessToken_RequestEntity"/>类型的实例
        /// </summary>
        static Gitee_AccessToken_RequestEntity()
        {
            var provider = Ioc.Create<IGiteeConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            GiteeConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 注册应用时的获取的client_id
        /// </summary>
        [Required]
        public string client_id { get; set; } = GiteeConfig.APPID;

        /// <summary>
        /// 注册应用时的获取的client_secret。
        /// </summary>
        [Required]
        public string client_secret { get; set; } = GiteeConfig.APPKey;

        /// <summary>
        /// 调用authorize获得的code值。
        /// </summary>
        [Required]
        public string code { get; set; }

        /// <summary>
        /// 回调地址，需需与注册应用里的回调地址一致。
        /// </summary>
        [Required]
        public string redirect_uri { get; set; } = GiteeConfig.Redirect_Uri;

        /// <summary>
        /// 固定值
        /// </summary>
        public string grant_type { get; set; } = "authorization_code";
    }
}
