﻿using Ding.Biz.OAuthLogin.Gitee.Configs;
using Ding.Helpers;
using System;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// Step1：获取authorize Code
    /// </summary>
    public class Gitee_Authorize_RequestEntity
    {
        /// <summary>
        /// DingTalk登录配置
        /// </summary>
        protected static readonly GiteeConfig GiteeConfig;

        /// <summary>
        /// 初始化一个<see cref="Gitee_Authorize_RequestEntity"/>类型的实例
        /// </summary>
        static Gitee_Authorize_RequestEntity()
        {
            var provider = Ioc.Create<IGiteeConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            GiteeConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 注册应用时的获取的client_id
        /// </summary>
        [Required]
        public string client_id { get; set; } = GiteeConfig.APPID;
        /// <summary>
        /// Gitee鉴权成功之后，重定向到网站
        /// </summary>
        [Required]
        public string redirect_uri { get; set; } = GiteeConfig.Redirect_Uri;

        /// <summary>
        /// 固定值，传 code
        /// </summary>
        public string response_type { get; set; } = "code";

        /// <summary>
        /// 说明：码云不支持该参数
        /// </summary>
        public string state { get; set; } = Guid.NewGuid().ToString("N");
    }
}
