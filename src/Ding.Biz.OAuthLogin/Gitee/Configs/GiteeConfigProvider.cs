﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.Gitee.Configs
{
    /// <summary>
    /// GitHub配置提供器
    /// </summary>
    public class GiteeConfigProvider : IGiteeConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly GiteeConfig _config;

        /// <summary>
        /// 初始化GitHub配置提供器
        /// </summary>
        /// <param name="config"></param>
        public GiteeConfigProvider(GiteeConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<GiteeConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
