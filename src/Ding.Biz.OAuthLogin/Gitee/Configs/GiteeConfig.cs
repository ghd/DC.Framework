﻿namespace Ding.Biz.OAuthLogin.Gitee.Configs
{
    /// <summary>
    /// 配置
    /// 
    /// 步骤：authorize => access_token => user
    /// </summary>
    public class GiteeConfig : ConfigBase
    {
        #region API请求接口

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize = "https://gitee.com/oauth/authorize";

        /// <summary>
        /// POST
        /// </summary>
        public static string API_AccessToken = "https://gitee.com/oauth/token";

        /// <summary>
        /// GET
        /// </summary>
        public static string API_User = "https://gitee.com/api/v5/user";

        #endregion
    }
}
