﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.Gitee.Configs
{
    /// <summary>
    /// AliPay配置提供器
    /// </summary>
    public interface IGiteeConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<GiteeConfig> GetConfigAsync();
    }
}
