﻿using Ding.Biz.OAuthLogin.Google.Configs;
using Ding.Helpers;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// access token 请求参数
    /// </summary>
    public class Google_AccessToken_RequestEntity
    {
        /// <summary>
        /// Google登录配置
        /// </summary>
        protected static readonly GoogleConfig GoogleConfig;

        /// <summary>
        /// 初始化一个<see cref="Google_AccessToken_RequestEntity"/>类型的实例
        /// </summary>
        static Google_AccessToken_RequestEntity()
        {
            var provider = Ioc.Create<IGoogleConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            GoogleConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 注册应用时的获取的client_id
        /// </summary>
        [Required]
        public string client_id { get; set; } = GoogleConfig.APPID;

        /// <summary>
        /// 注册应用时的获取的client_secret。
        /// </summary>
        [Required]
        public string client_secret { get; set; } = GoogleConfig.APPKey;

        /// <summary>
        /// 调用authorize获得的code值。
        /// </summary>
        [Required]
        public string code { get; set; }

        /// <summary>
        /// 回调地址，需需与注册应用里的回调地址一致。
        /// </summary>
        [Required]
        public string redirect_uri { get; set; } = GoogleConfig.Redirect_Uri;

        /// <summary>
        /// 固定值
        /// </summary>
        public string grant_type { get; set; } = "authorization_code";
    }
}
