﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.Google.Configs
{
    /// <summary>
    /// Google配置提供器
    /// </summary>
    public class GoogleConfigProvider : IGoogleConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly GoogleConfig _config;

        /// <summary>
        /// 初始化GitHub配置提供器
        /// </summary>
        /// <param name="config"></param>
        public GoogleConfigProvider(GoogleConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<GoogleConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
