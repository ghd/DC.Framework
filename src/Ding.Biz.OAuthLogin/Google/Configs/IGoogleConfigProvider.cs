﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.Google.Configs
{
    /// <summary>
    /// GitHub配置提供器
    /// </summary>
    public interface IGoogleConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<GoogleConfig> GetConfigAsync();
    }
}
