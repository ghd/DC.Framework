﻿using Ding.Biz.OAuthLogin.AliPay.Configs;
using Ding.Biz.OAuthLogin.DingTalk.Configs;
using Ding.Biz.OAuthLogin.Gitee.Configs;
using Ding.Biz.OAuthLogin.GitHub.Configs;
using Ding.Biz.OAuthLogin.Google.Configs;
using Ding.Biz.OAuthLogin.MicroSoft.Configs;
using Ding.Biz.OAuthLogin.QQ.Configs;
using Ding.Biz.OAuthLogin.StackOverflow.Configs;
using Ding.Biz.OAuthLogin.TaoBao.Configs;
using Ding.Biz.OAuthLogin.WeChat.Configs;
using Ding.Biz.OAuthLogin.Weibo.Configs;
using Ding.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// 登录工厂
    /// </summary>
    public class LoginFactory : ILoginFactory
    {
        /// <summary>
        /// 支付宝登录配置提供器
        /// </summary>
        protected readonly IAliPayConfigProvider _aliPayConfigProvider;

        /// <summary>
        /// 钉钉登录配置提供器
        /// </summary>
        protected readonly IDingTalkConfigProvider _dingTalkConfigProvider;

        /// <summary>
        /// 码云登录配置提供器
        /// </summary>
        protected readonly IGiteeConfigProvider _giteeConfigProvider;

        /// <summary>
        /// QQ登录配置提供器
        /// </summary>
        protected readonly IQQConfigProvider _qqConfigProvider;

        /// <summary>
        /// 微信登录配置提供器
        /// </summary>
        protected readonly IWeChatConfigProvider _wechatConfigProvider;

        /// <summary>
        /// GitHub登录配置提供器
        /// </summary>
        protected readonly IAliPayConfigProvider _githubConfigProvider;

        /// <summary>
        /// MicroSoft登录配置提供器
        /// </summary>
        protected readonly IMicroSoftConfigProvider _microsoftConfigProvider;

        /// <summary>
        /// Taobao登录配置提供器
        /// </summary>
        protected readonly ITaoBaoConfigProvider _taobaoConfigProvider;

        /// <summary>
        /// Weibo登录配置提供器
        /// </summary>
        protected readonly IWeiboConfigProvider _weibaoConfigProvider;

        public LoginFactory(IAliPayConfigProvider aliPayConfigProvider, IDingTalkConfigProvider dingTalkConfigProvider, IGiteeConfigProvider giteeConfigProvider, IQQConfigProvider qqConfigProvider, IWeChatConfigProvider wechatConfigProvider, IAliPayConfigProvider githubConfigProvider, IMicroSoftConfigProvider microsoftConfigProvider, ITaoBaoConfigProvider taobaoConfigProvider, IWeiboConfigProvider weibaoConfigProvider)
        {
            _aliPayConfigProvider = aliPayConfigProvider;
            _qqConfigProvider = qqConfigProvider;
            _giteeConfigProvider = giteeConfigProvider;
            _dingTalkConfigProvider = dingTalkConfigProvider;
            _wechatConfigProvider = wechatConfigProvider;
            _githubConfigProvider = githubConfigProvider;
            _microsoftConfigProvider = microsoftConfigProvider;
            _taobaoConfigProvider = taobaoConfigProvider;
            _weibaoConfigProvider = weibaoConfigProvider;
        }

        #region 支付宝登录
        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(AliPay_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                AliPayConfig.API_Authorize,
                "?app_id=",
                entity.app_id,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode(),
                "&scope=",
                entity.scope });
        }

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<AliPay_AccessToken_ResultEntity> AccessToken(AliPay_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            await Signature(entity);

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(AliPayConfig.API_Gateway + "?" + pars);

            AliPay_AccessToken_ResultEntity outmo = null;
            if (result.Contains("alipay_system_oauth_token_response"))
            {
                outmo = result.ToJObject()["alipay_system_oauth_token_response"].ToJson().ToEntity<AliPay_AccessToken_ResultEntity>();
            }

            if (result.Contains("error_response"))
            {
                outmo = result.ToJObject()["error_response"].ToJson().ToEntity<AliPay_AccessToken_ResultEntity>();
            }

            return outmo;
        }

        /// <summary>
        /// Step3：获取个人信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<AliPay_User_ResultEntity> User(AliPay_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            await Signature(entity);

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(AliPayConfig.API_Gateway + "?" + pars);

            AliPay_User_ResultEntity outmo = null;
            if (result.Contains("alipay_user_info_share_response"))
            {
                outmo = result.ToJObject()["alipay_user_info_share_response"].ToJson().ToEntity<AliPay_User_ResultEntity>();
            }

            if (result.Contains("error_response"))
            {
                outmo = result.ToJObject()["error_response"].ToJson().ToEntity<AliPay_User_ResultEntity>();
            }

            return outmo;
        }

        #region 签名

        /// <summary>
        /// 签名
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mo"></param>
        /// <param name="charset">编码，默认UTF8</param>
        /// <param name="signType">加密类型，默认RSA2</param>
        public async Task Signature<T>(T mo, string charset = null, string signType = "RSA2") where T : new()
        {
            var parameters = new Dictionary<string, string>();

            var pis = mo.GetType().GetProperties();
            foreach (var pi in pis)
            {
                string value = pi.GetValue(mo, null)?.ToString();
                parameters.Add(pi.Name, value);
            }

            // 第一步：把字典按Key的字母顺序排序
            IDictionary<string, string> sortedParams = new SortedDictionary<string, string>(parameters, StringComparer.Ordinal);
            IEnumerator<KeyValuePair<string, string>> dem = sortedParams.GetEnumerator();

            // 第二步：把所有参数名和参数值串在一起
            StringBuilder query = new StringBuilder("");
            while (dem.MoveNext())
            {
                string key = dem.Current.Key;
                string value = dem.Current.Value;
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                {
                    query.Append(key).Append("=").Append(value).Append("&");
                }
            }

            //待签名
            string data = query.ToString().TrimEnd('&');

            var config = await _aliPayConfigProvider.GetConfigAsync();
            config.CheckNotNull(nameof(config));

            //处理密钥
            RSACryptoServiceProvider rsaCsp = DecodeRSAPrivateKey(config.APPKey, signType);

            byte[] dataBytes;
            if (string.IsNullOrEmpty(charset))
            {
                dataBytes = Encoding.UTF8.GetBytes(data);
            }
            else
            {
                dataBytes = Encoding.GetEncoding(charset).GetBytes(data);
            }

            byte[] signatureBytes;
            if ("RSA2".Equals(signType))
            {
                signatureBytes = rsaCsp.SignData(dataBytes, "SHA256");
            }
            else
            {
                signatureBytes = rsaCsp.SignData(dataBytes, "SHA1");
            }

            //赋值签名
            foreach (var pi in pis)
            {
                if (pi.Name == "sign")
                {
                    pi.SetValue(mo, System.Convert.ToBase64String(signatureBytes), null);
                    break;
                }
            }
        }

        private RSACryptoServiceProvider DecodeRSAPrivateKey(string strKey, string signType)
        {
            byte[] privkey = System.Convert.FromBase64String(strKey);

            byte[] MODULUS, E, D, P, Q, DP, DQ, IQ;

            // --------- Set up stream to decode the asn.1 encoded RSA private key ------
            MemoryStream mem = new MemoryStream(privkey);
            BinaryReader binr = new BinaryReader(mem);  //wrap Memory Stream with BinaryReader for easy reading
            try
            {
                ushort twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();    //advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes != 0x0102) //version number
                    return null;
                byte bt = binr.ReadByte();
                if (bt != 0x00)
                    return null;


                //------ all private key components are Integer sequences ----
                int elems = GetIntegerSize(binr);
                MODULUS = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                E = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                D = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                P = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                Q = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DP = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DQ = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                IQ = binr.ReadBytes(elems);


                // ------- create RSACryptoServiceProvider instance and initialize with public key -----
                CspParameters CspParameters = new CspParameters();
                CspParameters.Flags = CspProviderFlags.UseMachineKeyStore;

                int bitLen = 1024;
                if ("RSA2".Equals(signType))
                {
                    bitLen = 2048;
                }

                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(bitLen, CspParameters);
                RSAParameters RSAparams = new RSAParameters();
                RSAparams.Modulus = MODULUS;
                RSAparams.Exponent = E;
                RSAparams.D = D;
                RSAparams.P = P;
                RSAparams.Q = Q;
                RSAparams.DP = DP;
                RSAparams.DQ = DQ;
                RSAparams.InverseQ = IQ;
                RSA.ImportParameters(RSAparams);
                return RSA;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                binr.Close();
            }
        }

        private int GetIntegerSize(BinaryReader binr)
        {
            byte bt = binr.ReadByte();
            if (bt != 0x02)     //expect integer
                return 0;
            bt = binr.ReadByte();
            int count;
            if (bt == 0x81)
                count = binr.ReadByte();    // data size in next byte
            else
                if (bt == 0x82)
            {
                byte highbyte = binr.ReadByte();
                byte lowbyte = binr.ReadByte();
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
                count = BitConverter.ToInt32(modint, 0);
            }
            else
            {
                count = bt;     // we already have the data size
            }

            while (binr.ReadByte() == 0x00)
            {   //remove high order zeros in data
                count -= 1;
            }
            binr.BaseStream.Seek(-1, SeekOrigin.Current);       //last ReadByte wasn't a removed zero, so back up a byte
            return count;
        }

        #endregion
        #endregion

        #region QQ登录
        /// <summary>
        /// Step1：获取Authorization Code
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizationHref(QQ_Authorization_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                QQConfig.API_Authorization_PC,
                "?client_id=",
                entity.client_id,
                "&response_type=",
                entity.response_type,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// Step2：通过Authorization Code获取Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public QQ_AccessToken_ResultEntity AccessToken(QQ_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            string result = HttpTo.Get(QQConfig.API_AccessToken_PC + "?" + pars);

            List<string> listPars = result.Split('&').ToList();
            var jo = new JObject();
            foreach (string item in listPars)
            {
                var items = item.Split('=').ToList();
                jo[items.FirstOrDefault()] = items.LastOrDefault();
            }

            var outmo = LoginBase.ResultOutput<QQ_AccessToken_ResultEntity>(Newtonsoft.Json.JsonConvert.SerializeObject(jo));

            return outmo;
        }

        /// <summary>
        /// Step3：获取用户OpenId
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public QQ_OpenId_ResultEntity OpenId(QQ_OpenId_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(QQConfig.API_OpenID_PC + "?" + pars);
            result = result.Replace("callback( ", "").Replace(" );", "");

            var outmo = LoginBase.ResultOutput<QQ_OpenId_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// Step4：获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public QQ_OpenId_get_user_info_ResultEntity OpenId_Get_User_Info(QQ_OpenAPI_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(QQConfig.API_Get_User_Info + "?" + pars);

            var outmo = LoginBase.ResultOutput<QQ_OpenId_get_user_info_ResultEntity>(result.Replace("\r\n", ""));

            return outmo;
        }
        #endregion

        #region 微信登录
        /// <summary>
        /// Step1：获取Authorization Code
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizationHref(WeChat_Authorization_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                WeChatConfig.API_Authorization,
                "?appid=",
                entity.appid,
                "&response_type=",
                entity.response_type,
                "&scope=",
                entity.scope,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// Step2：通过Authorization Code获取Access Token、openid
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WeChat_AccessToken_ResultEntity AccessToken(WeChat_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(WeChatConfig.API_AccessToken + "?" + pars);

            var outmo = LoginBase.ResultOutput<WeChat_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// Step3：获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WeChat_OpenId_get_user_info_ResultEntity Get_User_Info(WeChat_OpenAPI_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(WeChatConfig.API_UserInfo + "?" + pars);

            var outmo = LoginBase.ResultOutput<WeChat_OpenId_get_user_info_ResultEntity>(result.Replace("\r\n", ""));

            return outmo;
        }
        #endregion

        #region 钉钉
        /// <summary>
        /// 获取access_token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public DingTalk_AccessToken_ResultEntity AccessToken(DingTalk_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            var result = HttpTo.Get(DingTalkConfig.API_AccessToken + "?" + pars);

            var outmo = LoginBase.ResultOutput<DingTalk_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity">签名参数</param>
        /// <param name="code">临时授权码</param>
        /// <returns></returns>
        public DingTalk_User_ResultEntity User(DingTalk_User_RequestEntity entity, string code)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var hwr = HttpTo.HWRequest(DingTalkConfig.API_User + "?" + pars, "POST", new { tmp_auth_code = code }.ToJson());
            hwr.ContentType = "application/json";
            var result = HttpTo.Url(hwr);

            var ro = result.ToJObject();
            if (ro["errcode"].ToString() == "0")
            {
                result = result.ToJObject()["user_info"].ToJson();
            }
            else
            {
                return null;
            }

            var outmo = LoginBase.ResultOutput<DingTalk_User_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 请求授权地址,扫码登录
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref_ScanCode(DingTalk_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                DingTalkConfig.API_Authorize_ScanCode,
                "?appid=",
                entity.appid,
                "&response_type=",
                entity.response_type,
                "&scope=",
                entity.scope,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// 请求授权地址,密码登录
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref_Password(DingTalk_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                DingTalkConfig.API_Authorize_Password,
                "?appid=",
                entity.appid,
                "&response_type=",
                entity.response_type,
                "&scope=",
                entity.scope,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }
        #endregion

        #region 码云
        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(Gitee_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                GiteeConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&response_type=",
                entity.response_type,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Gitee_AccessToken_ResultEntity AccessToken(Gitee_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var result = HttpTo.Post(GiteeConfig.API_AccessToken, pars);

            var outmo = LoginBase.ResultOutput<Gitee_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Gitee_User_ResultEntity User(Gitee_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var result = HttpTo.Get(GiteeConfig.API_User + "?" + pars);

            var outmo = LoginBase.ResultOutput<Gitee_User_ResultEntity>(result);

            return outmo;
        }
        #endregion

        #region GitHub
        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(GitHub_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                GitHubConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&scope=",
                entity.scope.ToEncode(),
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public GitHub_AccessToken_ResultEntity AccessToken(GitHub_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var hwr = HttpTo.HWRequest(GitHubConfig.API_AccessToken, "POST", pars);
            hwr.Accept = "application/json";//application/xml
            string result = HttpTo.Url(hwr);

            var outmo = LoginBase.ResultOutput<GitHub_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public GitHub_User_ResultEntity User(GitHub_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var hwr = HttpTo.HWRequest(GitHubConfig.API_User + "?" + pars);
            hwr.UserAgent = entity.ApplicationName;
            string result = HttpTo.Url(hwr);

            var outmo = LoginBase.ResultOutput<GitHub_User_ResultEntity>(result, new List<string> { "plan" });

            return outmo;
        }
        #endregion

        #region Google
        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(Google_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                GoogleConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&response_type=",
                entity.response_type,
                "&scope=",
                entity.scope.ToEncode(),
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Google_AccessToken_ResultEntity AccessToken(Google_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var result = HttpTo.Post(GoogleConfig.API_AccessToken, pars);

            var outmo = LoginBase.ResultOutput<Google_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Google_User_ResultEntity User(Google_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var result = HttpTo.Get(GoogleConfig.API_User + "?" + pars);

            var outmo = LoginBase.ResultOutput<Google_User_ResultEntity>(result);

            return outmo;
        }
        #endregion

        #region MicroSoft
        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(MicroSoft_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                MicroSoftConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&scope=",
                entity.scope,
                "&response_type=",
                entity.response_type,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public MicroSoft_AccessToken_ResultEntity AccessToken(MicroSoft_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            string result = HttpTo.Post(MicroSoftConfig.API_AccessToken, pars);

            var outmo = LoginBase.ResultOutput<MicroSoft_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public MicroSoft_User_ResultEntity User(MicroSoft_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);

            var hwr = HttpTo.HWRequest(MicroSoftConfig.API_User + "?" + pars);
            hwr.ContentType = null;
            string result = HttpTo.Url(hwr);
            var outmo = LoginBase.ResultOutput<MicroSoft_User_ResultEntity>(result, new List<string> { "emails" });

            return outmo;
        }
        #endregion

        #region TaoBao
        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(TaoBao_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                TaoBaoConfig.API_Authorize,
                "?response_type=",
                entity.response_type,
                "&client_id=",
                entity.client_id,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode(),
                "&state=",
                entity.state,
                "&view=",
                entity.view});
        }

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TaoBao_AccessToken_ResultEntity AccessToken(TaoBao_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Post(TaoBaoConfig.API_AccessToken, pars);
            var outmo = LoginBase.ResultOutput<TaoBao_AccessToken_ResultEntity>(result);

            return outmo;
        }
        #endregion

        #region Weibo
        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(Weibo_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                WeiboConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&response_type=",
                entity.response_type,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Weibo_AccessToken_ResultEntity AccessToken(Weibo_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Post(WeiboConfig.API_AccessToken, pars);

            var outmo = LoginBase.ResultOutput<Weibo_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// Step3：查询用户access_token的授权相关信息，包括授权时间，过期时间和scope权限。
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Weibo_GetTokenInfo_ResultEntity GetTokenInfo(Weibo_GetTokenInfo_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Post(WeiboConfig.API_GetTokenInfo, pars);

            var outmo = LoginBase.ResultOutput<Weibo_GetTokenInfo_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// Step4：根据用户ID获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Weibo_UserShow_ResultEntity UserShow(Weibo_UserShow_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(WeiboConfig.API_UserShow + "?" + pars);

            var outmo = LoginBase.ResultOutput<Weibo_UserShow_ResultEntity>(result, new List<string> { "status" });

            return outmo;
        }
        #endregion

        #region StackOverflow
        /// <summary>
        /// 获取access_token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public StackOverflow_AccessToken_ResultEntity AccessToken(StackOverflow_AccessToken_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            var result = HttpTo.Post(StackOverflowConfig.API_AccessToken, pars);

            result = "{\"" + result.Replace("=", "\":\"").Replace("&", "\",\"") + "\"}";

            var outmo = LoginBase.ResultOutput<StackOverflow_AccessToken_ResultEntity>(result);

            return outmo;
        }

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity">签名参数</param>
        /// <returns></returns>
        public StackOverflow_User_ResultEntity User(StackOverflow_User_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            string pars = LoginBase.EntityToPars(entity);
            string result = HttpTo.Get(StackOverflowConfig.API_User + "?" + pars);

            StackOverflow_User_ResultEntity outmo = null;

            var jo = result.ToJObject();
            if (jo.ContainsKey("items"))
            {
                outmo = LoginBase.ResultOutput<StackOverflow_User_ResultEntity>(jo["items"][0].ToJson());
            }

            return outmo;
        }

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string AuthorizeHref(StackOverflow_Authorize_RequestEntity entity)
        {
            if (!LoginBase.IsValid(entity))
            {
                return null;
            }

            return string.Concat(new string[] {
                StackOverflowConfig.API_Authorize,
                "?client_id=",
                entity.client_id,
                "&scope=",
                entity.scope,
                "&state=",
                entity.state,
                "&redirect_uri=",
                entity.redirect_uri.ToEncode()});
        }
        #endregion
    }
}
