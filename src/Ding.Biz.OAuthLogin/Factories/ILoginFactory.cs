﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// 登录工厂
    /// </summary>
    public interface ILoginFactory
    {
        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(AliPay_Authorize_RequestEntity entity);

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<AliPay_AccessToken_ResultEntity> AccessToken(AliPay_AccessToken_RequestEntity entity);

        /// <summary>
        /// Step3：获取个人信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<AliPay_User_ResultEntity> User(AliPay_User_RequestEntity entity);

        /// <summary>
        /// 获取access_token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        DingTalk_AccessToken_ResultEntity AccessToken(DingTalk_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity">签名参数</param>
        /// <param name="code">临时授权码</param>
        /// <returns></returns>
        DingTalk_User_ResultEntity User(DingTalk_User_RequestEntity entity, string code);

        /// <summary>
        /// 请求授权地址,扫码登录
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref_ScanCode(DingTalk_Authorize_RequestEntity entity);

        /// <summary>
        /// 请求授权地址,密码登录
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref_Password(DingTalk_Authorize_RequestEntity entity);

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(Gitee_Authorize_RequestEntity entity);

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Gitee_AccessToken_ResultEntity AccessToken(Gitee_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Gitee_User_ResultEntity User(Gitee_User_RequestEntity entity);

        /// <summary>
        /// Step1：获取Authorization Code
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizationHref(QQ_Authorization_RequestEntity entity);

        /// <summary>
        /// Step2：通过Authorization Code获取Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        QQ_AccessToken_ResultEntity AccessToken(QQ_AccessToken_RequestEntity entity);

        /// <summary>
        /// Step3：获取用户OpenId
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        QQ_OpenId_ResultEntity OpenId(QQ_OpenId_RequestEntity entity);

        /// <summary>
        /// Step4：获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        QQ_OpenId_get_user_info_ResultEntity OpenId_Get_User_Info(QQ_OpenAPI_RequestEntity entity);

        /// <summary>
        /// Step1：获取Authorization Code
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizationHref(WeChat_Authorization_RequestEntity entity);

        /// <summary>
        /// Step2：通过Authorization Code获取Access Token、openid
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        WeChat_AccessToken_ResultEntity AccessToken(WeChat_AccessToken_RequestEntity entity);

        /// <summary>
        /// Step3：获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        WeChat_OpenId_get_user_info_ResultEntity Get_User_Info(WeChat_OpenAPI_RequestEntity entity);

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(GitHub_Authorize_RequestEntity entity);

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        GitHub_AccessToken_ResultEntity AccessToken(GitHub_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        GitHub_User_ResultEntity User(GitHub_User_RequestEntity entity);

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(Google_Authorize_RequestEntity entity);

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Google_AccessToken_ResultEntity AccessToken(Google_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Google_User_ResultEntity User(Google_User_RequestEntity entity);

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(MicroSoft_Authorize_RequestEntity entity);

        /// <summary>
        /// 获取 access token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        MicroSoft_AccessToken_ResultEntity AccessToken(MicroSoft_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        MicroSoft_User_ResultEntity User(MicroSoft_User_RequestEntity entity);

        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(TaoBao_Authorize_RequestEntity entity);

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TaoBao_AccessToken_ResultEntity AccessToken(TaoBao_AccessToken_RequestEntity entity);

        /// <summary>
        /// Step1：请求用户授权Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(Weibo_Authorize_RequestEntity entity);

        /// <summary>
        /// Step2：获取授权过的Access Token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Weibo_AccessToken_ResultEntity AccessToken(Weibo_AccessToken_RequestEntity entity);

        /// <summary>
        /// Step3：查询用户access_token的授权相关信息，包括授权时间，过期时间和scope权限。
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Weibo_GetTokenInfo_ResultEntity GetTokenInfo(Weibo_GetTokenInfo_RequestEntity entity);

        /// <summary>
        /// Step4：根据用户ID获取用户信息
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Weibo_UserShow_ResultEntity UserShow(Weibo_UserShow_RequestEntity entity);

        /// <summary>
        /// 获取access_token
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        StackOverflow_AccessToken_ResultEntity AccessToken(StackOverflow_AccessToken_RequestEntity entity);

        /// <summary>
        /// 获取 用户信息
        /// </summary>
        /// <param name="entity">签名参数</param>
        /// <returns></returns>
        StackOverflow_User_ResultEntity User(StackOverflow_User_RequestEntity entity);

        /// <summary>
        /// 请求授权地址
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string AuthorizeHref(StackOverflow_Authorize_RequestEntity entity);
    }
}
