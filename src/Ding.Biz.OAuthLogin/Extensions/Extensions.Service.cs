﻿using Ding.Biz.OAuthLogin.QQ.Configs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using Ding.Biz.OAuthLogin.WeChat.Configs;
using Ding.Biz.OAuthLogin.GitHub.Configs;
using Ding.Biz.OAuthLogin.MicroSoft.Configs;
using Ding.Biz.OAuthLogin.Weibo.Configs;
using Ding.Biz.OAuthLogin.AliPay.Configs;
using Ding.Biz.OAuthLogin.DingTalk.Configs;
using Ding.Biz.OAuthLogin.Gitee.Configs;
using Ding.Biz.OAuthLogin.Google.Configs;
using Ding.Biz.OAuthLogin.TaoBao.Configs;
using Ding.Biz.OAuthLogin.StackOverflow.Configs;

namespace Ding.Biz.OAuthLogin.Extensions
{
    /// <summary>
    /// 登录扩展
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// 注册登录操作
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="setupAction">配置操作</param>
        public static void AddLogin(this IServiceCollection services, Action<LoginOptions> setupAction)
        {
            var options = new LoginOptions();
            setupAction?.Invoke(options);
            services.TryAddSingleton<IAliPayConfigProvider>(new AliPayConfigProvider(options.AliPayOptions));
            services.TryAddSingleton<IDingTalkConfigProvider>(new DingTalkConfigProvider(options.DingTalkOptions));
            services.TryAddSingleton<IGiteeConfigProvider>(new GiteeConfigProvider(options.GiteeOptions));
            services.TryAddSingleton<IQQConfigProvider>(new QQConfigProvider(options.QqOptions));
            services.TryAddSingleton<IWeChatConfigProvider>(new WeChatConfigProvider(options.WeChatOptions));
            services.TryAddSingleton<IGitHubConfigProvider>(new GitHubConfigProvider(options.GitHubOptions));
            services.TryAddSingleton<IGoogleConfigProvider>(new GoogleConfigProvider(options.GoogleOptions));
            services.TryAddSingleton<IMicroSoftConfigProvider>(new MicroSoftConfigProvider(options.MicroSoftConfig));
            services.TryAddSingleton<ITaoBaoConfigProvider>(new TaoBaoConfigProvider(options.TaobaoConfig));
            services.TryAddSingleton<IWeiboConfigProvider>(new WeiboConfigProvider(options.WeiboConfig));
            services.TryAddSingleton<IStackOverflowConfigProvider>(new StackOverflowConfigProvider(options.StackOverflowConfig));
            services.TryAddScoped<ILoginFactory, LoginFactory>();
        }
    }
}
