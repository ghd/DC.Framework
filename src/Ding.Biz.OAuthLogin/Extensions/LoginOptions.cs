﻿using Ding.Biz.OAuthLogin.AliPay.Configs;
using Ding.Biz.OAuthLogin.DingTalk.Configs;
using Ding.Biz.OAuthLogin.Gitee.Configs;
using Ding.Biz.OAuthLogin.GitHub.Configs;
using Ding.Biz.OAuthLogin.Google.Configs;
using Ding.Biz.OAuthLogin.MicroSoft.Configs;
using Ding.Biz.OAuthLogin.QQ.Configs;
using Ding.Biz.OAuthLogin.StackOverflow.Configs;
using Ding.Biz.OAuthLogin.TaoBao.Configs;
using Ding.Biz.OAuthLogin.WeChat.Configs;
using Ding.Biz.OAuthLogin.Weibo.Configs;

namespace Ding.Biz.OAuthLogin.Extensions
{
    /// <summary>
    /// 登录配置
    /// </summary>
    public class LoginOptions
    {
        /// <summary>
        /// AliPay配置
        /// </summary>
        public AliPayConfig AliPayOptions { get; set; } = new AliPayConfig();

        /// <summary>
        /// DingTalk配置
        /// </summary>
        public DingTalkConfig DingTalkOptions { get; set; } = new DingTalkConfig();

        /// <summary>
        /// Gitee配置
        /// </summary>
        public GiteeConfig GiteeOptions { get; set; } = new GiteeConfig();

        /// <summary>
        /// QQ配置
        /// </summary>
        public QQConfig QqOptions { get; set; } = new QQConfig();

        /// <summary>
        /// WeChat配置
        /// </summary>
        public WeChatConfig WeChatOptions { get; set; } = new WeChatConfig();

        /// <summary>
        /// GitHub配置
        /// </summary>
        public GitHubConfig GitHubOptions { get; set; } = new GitHubConfig();

        /// <summary>
        /// Google配置
        /// </summary>
        public GoogleConfig GoogleOptions { get; set; } = new GoogleConfig();

        /// <summary>
        /// MicroSoft配置
        /// </summary>
        public MicroSoftConfig MicroSoftConfig { get; set; } = new MicroSoftConfig();

        /// <summary>
        /// TaoBao配置
        /// </summary>
        public TaoBaoConfig TaobaoConfig { get; set; } = new TaoBaoConfig();

        /// <summary>
        /// Weibo配置
        /// </summary>
        public WeiboConfig WeiboConfig { get; set; } = new WeiboConfig();

        /// <summary>
        /// StackOverflow配置
        /// </summary>
        public StackOverflowConfig StackOverflowConfig { get; set; } = new StackOverflowConfig();
    }
}
