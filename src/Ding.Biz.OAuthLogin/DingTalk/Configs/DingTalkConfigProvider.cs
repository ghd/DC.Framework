﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.DingTalk.Configs
{
    /// <summary>
    /// GitHub配置提供器
    /// </summary>
    public class DingTalkConfigProvider : IDingTalkConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly DingTalkConfig _config;

        /// <summary>
        /// 初始化GitHub配置提供器
        /// </summary>
        /// <param name="config"></param>
        public DingTalkConfigProvider(DingTalkConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<DingTalkConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
