﻿namespace Ding.Biz.OAuthLogin.DingTalk.Configs
{
    /// <summary>
    /// 配置
    /// 
    /// 步骤：authorize => user
    /// </summary>
    public class DingTalkConfig : ConfigBase
    {
        #region API请求接口

        // 扫码模式

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize_ScanCode = "https://oapi.dingtalk.com/connect/qrconnect";

        // 密码模式

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize_Password = "https://oapi.dingtalk.com/connect/oauth2/sns_authorize";

        /// <summary>
        /// GET
        /// </summary>
        public static string API_AccessToken = "https://oapi.dingtalk.com/sns/gettoken";

        /// <summary>
        /// POST
        /// </summary>
        public static string API_User = "https://oapi.dingtalk.com/sns/getuserinfo_bycode";

        #endregion
    }
}
