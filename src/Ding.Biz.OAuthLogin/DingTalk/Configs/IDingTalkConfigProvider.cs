﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.DingTalk.Configs
{
    /// <summary>
    /// AliPay配置提供器
    /// </summary>
    public interface IDingTalkConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<DingTalkConfig> GetConfigAsync();
    }
}
