﻿using Ding.Biz.OAuthLogin.DingTalk.Configs;
using Ding.Helpers;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// access token 请求参数
    /// </summary>
    public class DingTalk_AccessToken_RequestEntity
    {
        /// <summary>
        /// DingTalk登录配置
        /// </summary>
        protected static readonly DingTalkConfig DingTalkConfig;

        /// <summary>
        /// 初始化一个<see cref="DingTalk_AccessToken_RequestEntity"/>类型的实例
        /// </summary>
        static DingTalk_AccessToken_RequestEntity()
        {
            var provider = Ioc.Create<IDingTalkConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            DingTalkConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 应用的唯一标识key
        /// </summary>
        [Required]
        public string appid { get; set; } = DingTalkConfig.APPID;

        /// <summary>
        /// 应用的密钥
        /// </summary>
        [Required]
        public string appsecret { get; set; } = DingTalkConfig.APPKey;
    }
}
