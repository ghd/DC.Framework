﻿using Ding.Biz.OAuthLogin.DingTalk.Configs;
using Ding.Helpers;
using System;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// user
    /// </summary>
    public class DingTalk_User_RequestEntity
    {
        /// <summary>
        /// DingTalk登录配置
        /// </summary>
        protected static readonly DingTalkConfig DingTalkConfig;

        /// <summary>
        /// 初始化一个<see cref="DingTalk_User_RequestEntity"/>类型的实例
        /// </summary>
        static DingTalk_User_RequestEntity()
        {
            var provider = Ioc.Create<IDingTalkConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            DingTalkConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 构造，签名
        /// </summary>
        public DingTalk_User_RequestEntity()
        {
            signature = CalcTo.HMAC_SHA256(timestamp, DingTalkConfig.APPKey);
        }

        /// <summary>
        /// appid
        /// </summary>
        [Required]
        public string accessKey { get; set; } = DingTalkConfig.APPID;

        /// <summary>
        /// 当前时间戳，单位是毫秒
        /// </summary>
        [Required]
        public string timestamp { get; set; } = DateTime.Now.ToTimestamp(true).ToString();

        /// <summary>
        /// 通过appSecret计算出来的签名值
        /// </summary>
        [Required]
        public string signature { get; set; }
    }
}
