﻿using Ding.Biz.OAuthLogin.StackOverflow.Configs;
using Ding.Helpers;
using System;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// Step1：获取authorize Code
    /// </summary>
    public class StackOverflow_Authorize_RequestEntity
    {
        /// <summary>
        /// StackOverflow登录配置
        /// </summary>
        protected static readonly StackOverflowConfig StackOverflowConfig;

        /// <summary>
        /// 初始化一个<see cref="StackOverflow_Authorize_RequestEntity"/>类型的实例
        /// </summary>
        static StackOverflow_Authorize_RequestEntity()
        {
            var provider = Ioc.Create<IStackOverflowConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            StackOverflowConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 注册应用时的获取的client_id
        /// </summary>
        [Required]
        public string client_id { get; set; } = StackOverflowConfig.APPID;

        /// <summary>
        /// github鉴权成功之后，重定向到网站
        /// </summary>
        [Required]
        public string redirect_uri { get; set; } = StackOverflowConfig.Redirect_Uri;

        /// <summary>
        /// 自己设定，用于防止跨站请求伪造攻击
        /// </summary>
        [Required]
        public string state { get; set; } = Guid.NewGuid().ToString("N");

        /// <summary>
        /// 
        /// </summary>
        public string scope { get; set; } = "";
    }
}
