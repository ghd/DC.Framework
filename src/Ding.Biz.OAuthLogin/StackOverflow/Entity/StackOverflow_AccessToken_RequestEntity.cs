﻿using Ding.Biz.OAuthLogin.StackOverflow.Configs;
using Ding.Helpers;

namespace Ding.Biz.OAuthLogin
{
    /// <summary>
    /// access token 请求参数
    /// </summary>
    public class StackOverflow_AccessToken_RequestEntity
    {
        /// <summary>
        /// StackOverflow登录配置
        /// </summary>
        protected static readonly StackOverflowConfig StackOverflowConfig;

        /// <summary>
        /// 初始化一个<see cref="StackOverflow_AccessToken_RequestEntity"/>类型的实例
        /// </summary>
        static StackOverflow_AccessToken_RequestEntity()
        {
            var provider = Ioc.Create<IStackOverflowConfigProvider>();
            provider.CheckNotNull(nameof(provider));
            StackOverflowConfig = provider.GetConfigAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 注册应用时的获取的client_id
        /// </summary>
        [Required]
        public string client_id { get; set; } = StackOverflowConfig.APPID;

        /// <summary>
        /// 应用的密钥
        /// </summary>
        [Required]
        public string client_secret { get; set; } = StackOverflowConfig.APPKey;

        /// <summary>
        /// 授权得到的code
        /// </summary>
        [Required]
        public string code { get; set; }

        /// <summary>
        /// 回调地址
        /// </summary>
        [Required]
        public string redirect_uri { get; set; } = StackOverflowConfig.Redirect_Uri;
    }
}
