﻿namespace Ding.Biz.OAuthLogin.StackOverflow.Configs
{
    /// <summary>
    /// 配置
    /// 
    /// 步骤：authorize => user
    /// </summary>
    public class StackOverflowConfig : ConfigBase
    {
        #region API请求接口

        /// <summary>
        /// GET
        /// </summary>
        public static string API_Authorize = "https://stackoverflow.com/oauth";

        /// <summary>
        /// GET
        /// </summary>
        public static string API_AccessToken = "https://stackoverflow.com/oauth/access_token";

        /// <summary>
        /// POST
        /// </summary>
        public static string API_User = "https://api.stackexchange.com/2.2/me";

        #endregion

        /// <summary>
        /// Key
        /// </summary>
        public static string Key = "";
    }
}
