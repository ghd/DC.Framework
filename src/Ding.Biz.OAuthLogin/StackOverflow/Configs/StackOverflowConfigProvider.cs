﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.StackOverflow.Configs
{
    /// <summary>
    /// StackOverflow配置提供器
    /// </summary>
    public class StackOverflowConfigProvider : IStackOverflowConfigProvider
    {
        /// <summary>
        /// 配置
        /// </summary>
        private readonly StackOverflowConfig _config;

        /// <summary>
        /// 初始化StackOverflow配置提供器
        /// </summary>
        /// <param name="config"></param>
        public StackOverflowConfigProvider(StackOverflowConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        public Task<StackOverflowConfig> GetConfigAsync()
        {
            return Task.FromResult(_config);
        }
    }
}
