﻿using System.Threading.Tasks;

namespace Ding.Biz.OAuthLogin.StackOverflow.Configs
{
    /// <summary>
    /// StackOverflow配置提供器
    /// </summary>
    public interface IStackOverflowConfigProvider
    {
        /// <summary>
        /// 获取配置
        /// </summary>
        /// <returns></returns>
        Task<StackOverflowConfig> GetConfigAsync();
    }
}
