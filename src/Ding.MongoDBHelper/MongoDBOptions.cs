﻿namespace Ding.MongoDB
{
    public class MongoDBOptions
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string DataBase { get; set; }

        /// <summary>
        /// 是否SSL连接
        /// </summary>
        public bool IsSSL { get; set; }
    }
}
