﻿#if __WIN__

namespace Ding.Tools.Mvc.Internal
{
    public struct ByteRange
    {
        public long Start { get; set; }
        public long End { get; set; }
    }
}
#endif