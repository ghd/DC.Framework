﻿namespace Ding.Localization
{
    public class LangOptions
    {
        /// <summary>
        /// 设定当前语言
        /// </summary>
        public string CurrentLanguage { get; set; }
    }
}
