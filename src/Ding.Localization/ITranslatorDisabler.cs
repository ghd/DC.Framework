﻿namespace Ding.Localization
{
    public interface ITranslatorDisabler
    {
        bool IsDisabled();
    }
}
