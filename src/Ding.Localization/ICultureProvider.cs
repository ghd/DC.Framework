﻿namespace Ding.Localization
{
    public interface ICultureProvider
    {
        string DetermineCulture();
    }
}
