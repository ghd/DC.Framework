﻿namespace Ding.HttpHelper.Item
{
    /// <summary>
    /// 图片对象
    /// </summary>
    public class ImgItem
    {
        /// <summary>
        /// 图片网址
        /// </summary>
        public string Src { get; set; }
        /// <summary>
        /// 图片标签Html
        /// </summary>
        public string Html { get; set; }
#if __WIN__
        /// <summary>
        /// 图片标签Html
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 图片标签Html
        /// </summary>
        public string alt { get; set; }
#endif
    }
}
