﻿using System.ComponentModel;

namespace Ding.Ui.Enums {
    /// <summary>
    /// 单选按钮样式
    /// </summary>
    public enum RadioButtonStyle {
        /// <summary>
        /// 描边
        /// </summary>
        [Description( "outline" )]
        Outline,
        /// <summary>
        /// 填色
        /// </summary>
        [Description( "solid" )]
        Solid
    }
}