﻿using System.ComponentModel;

namespace Ding.Ui.Enums {
    /// <summary>
    /// 方向
    /// </summary>
    public enum Orientation {
        /// <summary>
        /// 左
        /// </summary>
        [Description( "left" )]
        Left,
        /// <summary>
        /// 右
        /// </summary>
        [Description( "right" )]
        Right
    }
}
