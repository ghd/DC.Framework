﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro下拉列表包装器生成器
    /// </summary>
    public class SelectWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro下拉列表包装器生成器
        /// </summary>
        public SelectWrapperBuilder( ) : base( "x-select" ) {
        }
    }
}
