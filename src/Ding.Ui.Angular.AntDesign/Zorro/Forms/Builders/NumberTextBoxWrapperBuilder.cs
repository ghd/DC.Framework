﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro数字文本框包装器生成器
    /// </summary>
    public class NumberTextBoxWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro数字文本框包装器生成器
        /// </summary>
        public NumberTextBoxWrapperBuilder() : base( "x-number-textbox" ) {
        }
    }
}
