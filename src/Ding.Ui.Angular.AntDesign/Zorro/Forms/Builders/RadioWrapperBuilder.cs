﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro单选框包装器生成器
    /// </summary>
    class RadioWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro单选框包装器生成器
        /// </summary>
        public RadioWrapperBuilder( ) : base( "x-radio" ) {
        }
    }
}
