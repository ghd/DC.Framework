﻿namespace Ding.Ui.Zorro.Tags.Builders {
    /// <summary>
    /// NgZorro标签生成器
    /// </summary>
    public class ZorroTagBuilder : Ding.Ui.Builders.TagBuilder {
        /// <summary>
        /// 初始化标签生成器
        /// </summary>
        public ZorroTagBuilder() : base( "nz-tag" ) {
        }
    }
}