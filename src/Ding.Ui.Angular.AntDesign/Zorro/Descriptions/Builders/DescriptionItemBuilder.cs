﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Descriptions.Builders {
    /// <summary>
    /// NgZorro描述列表项生成器
    /// </summary>
    public class DescriptionItemBuilder : TagBuilder {
        /// <summary>
        /// 初始化描述列表项生成器
        /// </summary>
        public DescriptionItemBuilder() : base( "nz-descriptions-item" ) {
        }
    }
}