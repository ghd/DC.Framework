﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Messages.Builders {
    /// <summary>
    /// NgZorro警告提示生成器
    /// </summary>
    public class AlertBuilder : TagBuilder {
        /// <summary>
        /// 初始化警告提示生成器
        /// </summary>
        public AlertBuilder() : base( "nz-alert" ) {
        }
    }
}