﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Trees.Builders {
    /// <summary>
    /// NgZorro树形选择包装器生成器
    /// </summary>
    public class TreeSelectWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro树形选择包装器生成器
        /// </summary>
        public TreeSelectWrapperBuilder() : base( "x-tree-select" ) {
        }
    }
}
