﻿namespace Ding.Printer.UnicodeToZPL
{
    public class CompressCodeInfo
    {
        public CompressCodeInfo(string code, int count)
        {
            Code = code;
            Count = count;
        }

        public string Code { get; set; }

        public int Count { get; set; }
    }
}
