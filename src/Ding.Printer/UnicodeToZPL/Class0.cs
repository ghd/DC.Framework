﻿using System.Collections.Generic;
using System.Text;

namespace Ding.Printer.UnicodeToZPL
{
    internal class Class0
    {
        public static string smethod_0(string string_0)
        {
            List<CompressCodeInfo> list = new List<CompressCodeInfo>();
            char c = '.';
            int num = 0;
            for (int i = 0; i < string_0.Length; i++)
            {
                char c2 = string_0[i];
                if (c2 != c)
                {
                    if (num > 0)
                    {
                        list.Add(new CompressCodeInfo(c.ToString(), num));
                    }
                    num = 0;
                    c = c2;
                }
                num++;
                if (i == string_0.Length - 1 && num > 0)
                {
                    list.Add(new CompressCodeInfo(c.ToString(), num));
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < list.Count; j++)
            {
                CompressCodeInfo compressCodeInfo = list[j];
                if (compressCodeInfo.Count != 0)
                {
                    if (compressCodeInfo.Count == 1)
                    {
                        stringBuilder.Append(compressCodeInfo.Code);
                    }
                    else if (compressCodeInfo.Count <= 20)
                    {
                        stringBuilder.AppendFormat("{0}{1}", ((CompressCode)compressCodeInfo.Count).ToString(), compressCodeInfo.Code);
                    }
                    else
                    {
                        int num2 = 0;
                        while (compressCodeInfo.Count - num2 > 0)
                        {
                            if (compressCodeInfo.Count - num2 < 20)
                            {
                                stringBuilder.AppendFormat("{0}{1}", ((CompressCode)(compressCodeInfo.Count - num2)).ToString(), compressCodeInfo.Code);
                                break;
                            }
                            if (compressCodeInfo.Count - num2 >= 400)
                            {
                                num2 += 400;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.z.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 380)
                            {
                                num2 += 380;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.y.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 360)
                            {
                                num2 += 360;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.x.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 340)
                            {
                                num2 += 340;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.w.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 320)
                            {
                                num2 += 320;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.v.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 300)
                            {
                                num2 += 300;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.u.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 280)
                            {
                                num2 += 280;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.t.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 260)
                            {
                                num2 += 260;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.s.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 240)
                            {
                                num2 += 240;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.r.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 220)
                            {
                                num2 += 220;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.q.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 200)
                            {
                                num2 += 200;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.p.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 180)
                            {
                                num2 += 180;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.o.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 160)
                            {
                                num2 += 160;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.n.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 140)
                            {
                                num2 += 140;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.m.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 120)
                            {
                                num2 += 120;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.l.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 100)
                            {
                                num2 += 100;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.k.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 80)
                            {
                                num2 += 80;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.j.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 60)
                            {
                                num2 += 60;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.i.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 40)
                            {
                                num2 += 40;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.h.ToString(), compressCodeInfo.Code);
                            }
                            else if (compressCodeInfo.Count - num2 >= 20)
                            {
                                num2 += 20;
                                stringBuilder.AppendFormat("{0}{1}", CompressCode.g.ToString(), compressCodeInfo.Code);
                            }
                        }
                    }
                }
            }
            return stringBuilder.ToString();
        }

    }
}