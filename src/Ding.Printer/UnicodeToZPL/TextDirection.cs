﻿namespace Ding.Printer.UnicodeToZPL
{
    public enum TextDirection
    {
        零度,
        四十五度 = 45,
        九十度 = 90,
        一百八十度 = 180,
        二百七十度 = 270
    }
}
