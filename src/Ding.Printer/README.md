### **无压缩ZPL指令生成** 


```
               string content = this.richTextBoxInput.Text;
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("请输入文本内容");
                }
                string name = "name1";
                TextDirection textDirection = (TextDirection)this.comboBoxTextDirection.SelectedValue;
                string result = UnicodeToZPL.UnCompressZPL(content, name, this.richTextBoxInput.Font, textDirection);
                System.Text.StringBuilder builder = new StringBuilder();
                builder.AppendLine("^XA^LH0,0^PW800");
                builder.AppendLine("^FO50,50");
                builder.Append(result);
                builder.AppendLine("^FS");
                builder.Append("^XZ");
                this.richTextBoxResult.Text = builder.ToString();
```

###  **压缩ZPL指令生成** 


```
                string content = this.richTextBoxInput.Text;
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("请输入文本内容");
                }
                string name = "name1";
                TextDirection textDirection = (TextDirection)this.comboBoxTextDirection.SelectedValue;
                string result=UnicodeToZPL.CompressZPL(content, name, this.richTextBoxInput.Font, textDirection);
                System.Text.StringBuilder builder = new StringBuilder();
                builder.AppendLine("^XA^LH0,0^PW800");
                builder.AppendLine("^FO50,50");
                builder.Append(result);
                builder.AppendLine("^FS");
                builder.Append("^XZ");
                this.richTextBoxResult.Text = builder.ToString();
```

