﻿using Ding.Helpers.Internal;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Ding.Webs.Filters {
    /// <summary>
    /// 异常处理过滤器
    /// </summary>
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute {
        /// <summary>
        /// 异常处理
        /// </summary>
        public override void OnException( ExceptionContext context ) {
            context.ExceptionHandled = true;
            context.HttpContext.Response.StatusCode = 200;
            context.Result = new DResult( StateCode.Fail, context.Exception.GetPrompt() );
        }
    }
}
