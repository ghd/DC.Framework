﻿namespace Ding.Webs.ApiSign
{
    /// <summary>
    /// ApiSign选项配置
    /// </summary>
    public class ApiSignOptions
    {
        /// <summary>
        /// 前端交互加密公钥
        /// </summary>
        public string RsaPublicKey { get; set; }

        /// <summary>
        /// 请求超时时间
        /// </summary>
        public int RequestExpireTime { get; set; }

        /// <summary>
        /// 前端交互加密私钥
        /// </summary>
        public string RsaPrivateKey { get; set; }
    }
}
