﻿using Microsoft.AspNetCore.Http;

namespace Ding.Webs.ApiSign
{
    /// <summary>
    /// 请求签名头
    /// </summary>
    public class SignatureHeader
    {
        public SignatureHeader(IHeaderDictionary Headers)
        {
            if (Headers.ContainsKey("appid"))
            {
                AppId = Headers["appid"];
            }
            if (Headers.ContainsKey("appsecret"))
            {
                AppSecret = Headers["appsecret"];
            }
            if (Headers.ContainsKey("timestamp"))
            {
                Timestamp = Headers["timestamp"];
            }
            if (Headers.ContainsKey("nonce"))
            {
                Nonce = Headers["nonce"];
            }
            if (Headers.ContainsKey("signature"))
            {
                Signature = Headers["signature"];
            }
        }

        /// <summary>
        /// 检验参数
        /// </summary>
        /// <returns></returns>
        public bool Verify()
        {
            if (string.IsNullOrEmpty(AppId) || string.IsNullOrEmpty(Timestamp) || string.IsNullOrEmpty(Nonce) || string.IsNullOrEmpty(AppSecret) || string.IsNullOrEmpty(Signature))
                return false;
            return true;
        }

        public override string ToString()
        {
            return AppId + Timestamp + Nonce;
        }

        /// <summary>
        /// 应用系统代码
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 应用系统密钥
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; }

        /// <summary>
        /// 随机数
        /// </summary>
        public string Nonce { get; set; }

        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; }

    }
}
