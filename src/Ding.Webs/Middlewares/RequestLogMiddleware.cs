﻿using Ding.Logs.Extensions;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Ding.Webs.Middlewares
{
    /// <summary>
    /// 请求日志中间件
    /// </summary>
    public class RequestLogMiddleware
    {
        /// <summary>
        /// 允许日志写入响应内容
        /// </summary>
        public bool AllowWriteResponse { get; set; }

        /// <summary>
        /// 允许日志写入请求内容
        /// </summary>
        public bool AllowWriteRequest { get; set; }

        /// <summary>
        /// 下一个中间件
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// 初始化一个<see cref="RequestLogMiddleware"/>类型的实例
        /// </summary>
        /// <param name="next">方法</param>
        /// <param name="allowWriteRequest">允许日志写入请求内容</param>
        /// <param name="allowWriteResponse">允许日志写入响应内容</param>
        public RequestLogMiddleware(RequestDelegate next, bool allowWriteResponse, bool allowWriteRequest)
        {
            AllowWriteResponse = allowWriteResponse;
            AllowWriteRequest = allowWriteRequest;
            _next = next;
        }

        /// <summary>
        /// 执行方法
        /// </summary>
        /// <param name="context">Http上下文</param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.ToString().Contains("/health"))
            {
                await _next(context);
                return;
            }

            if (!AllowWriteRequest && !AllowWriteResponse)
            {
                var sw = new Stopwatch();
                sw.Start();
                await _next(context);
                sw.Stop();
                await WriteLog(context, sw);
            }
            else
            {
#if __CORE21__
            context.Request.EnableBuffering();
#else
                context.Request.EnableRewind();
#endif
                var originalBodyStream = context.Response.Body;
                using (var responseBody = new MemoryStream())
                {
                    context.Response.Body = responseBody;
                    var sw = new Stopwatch();
                    sw.Start();
                    await _next(context);
                    sw.Stop();
                    await WriteLog(context, sw);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
        }

        /// <summary>
        /// 记录请求日志
        /// </summary>
        /// <param name="context">Http上下文</param>
        /// <param name="stopwatch">计时器</param>
        /// <returns></returns>
        private async Task WriteLog(HttpContext context, Stopwatch stopwatch)
        {
            if (context == null)
            {
                return;
            }

            var log = Ding.Logs.Log.GetLog(this).Caption("请求日志中间件");
            var dc = new Dictionary<string, string>();
            dc.Add("请求方法", context.Request.Method);
            dc.Add("请求地址",
                    $"{context.Request.Scheme}://{context.Request.Host}{context.Request.Path}{context.Request.QueryString}");
            dc.Add("IP", context.Connection.RemoteIpAddress.ToString());
            dc.Add("请求耗时", $"{stopwatch.Elapsed.TotalMilliseconds} 毫秒");
            if (AllowWriteRequest)
            {
                dc.Add("请求内容", await FormatRequest(context.Request));
            }
            else
            {
                dc.Add("请求内容", "未开启写入请求功能，请设置UseRequestLog的AllowWriteRequest为True");
            }
            if (AllowWriteResponse)
            {
                dc.Add("响应内容", await FormatResponse(context.Response));
            }
            else
            {
                dc.Add("响应内容", "未开启写入响应功能，请设置UseRequestLog的AllowWriteResponse为True");
            }

            log.Content(dc);
            log.Info();
        }

        /// <summary>
        /// 格式化请求内容
        /// </summary>
        /// <param name="request">Http请求</param>
        /// <returns></returns>
        private async Task<string> FormatRequest(HttpRequest request)
        {
            request.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(request.Body).ReadToEndAsync();
            request.Body.Seek(0, SeekOrigin.Begin);
            return text?.Trim().Replace("\r", "").Replace("\n", "");
        }

        /// <summary>
        /// 格式化响应内容
        /// </summary>
        /// <param name="response">Http响应</param>
        /// <returns></returns>
        private async Task<string> FormatResponse(HttpResponse response)
        {
            if (response.HasStarted)
            {
                return string.Empty;
            }
            response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return text?.Trim().Replace("\r", "").Replace("\n", "");
        }
    }
}
