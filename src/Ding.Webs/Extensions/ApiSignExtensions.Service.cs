﻿using Ding.Webs.ApiSign;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ding.Webs.Extensions
{
    /// <summary>
    /// 服务扩展
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// 注册前后端交互接口检验
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="apiSignOptions">配置</param>
        public static IServiceCollection AddApiSign(this IServiceCollection services, Action<ApiSignOptions> apiSignOptions)
        {
            services.Configure(apiSignOptions);
            return services;
        }
    }
}
