﻿using Microsoft.AspNetCore.Authorization;

namespace Ding.Webs.Auth
{
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        public const string AuthenticationScheme = "AdminAuthenticationScheme";

        public AdminAuthorizeAttribute()
        {
            AuthenticationSchemes = AuthenticationScheme;
        }
    }
}
