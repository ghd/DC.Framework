﻿using Microsoft.AspNetCore.Authorization;

namespace Ding.Webs.Auth
{
    public class UserAuthorizeAttribute : AuthorizeAttribute
    {
        public const string AuthenticationScheme = "UserAuthenticationScheme";

        public UserAuthorizeAttribute()
        {
            AuthenticationSchemes = AuthenticationScheme;
        }
    }
}
