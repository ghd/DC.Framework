﻿using System;

namespace Ding.Payment.Alipay
{
    /// <summary>
    /// Alipay 基础对象。
    /// </summary>
    [Serializable]
    public abstract class AlipayObject
    {
    }
}
