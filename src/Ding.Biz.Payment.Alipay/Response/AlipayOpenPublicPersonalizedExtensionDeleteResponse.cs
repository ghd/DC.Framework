﻿namespace Ding.Payment.Alipay.Response
{
    /// <summary>
    /// AlipayOpenPublicPersonalizedExtensionDeleteResponse.
    /// </summary>
    public class AlipayOpenPublicPersonalizedExtensionDeleteResponse : AlipayResponse
    {
    }
}
