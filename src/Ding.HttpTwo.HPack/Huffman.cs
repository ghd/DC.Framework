﻿namespace HttpTwo.HPack
{
    public class Huffman
    {
        /// <summary>
        /// Huffman Decoder
        /// </summary>
        public static readonly HuffmanDecoder DECODER = new HuffmanDecoder(HPackUtil.HUFFMAN_CODES, HPackUtil.HUFFMAN_CODE_LENGTHS);

        /// <summary>
        /// Huffman Encoder
        /// </summary>
        public static readonly HuffmanEncoder ENCODER = new HuffmanEncoder(HPackUtil.HUFFMAN_CODES, HPackUtil.HUFFMAN_CODE_LENGTHS);

        private Huffman()
        {
            // utility class
        }
    }
}
