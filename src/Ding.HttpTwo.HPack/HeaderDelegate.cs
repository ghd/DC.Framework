﻿namespace HttpTwo.HPack
{
    /// <summary>
    /// EmitHeader is called by the decoder during header field emission.
    /// The name and value byte arrays must not be modified.
    /// </summary>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <param name="sensitive">If set to <c>true</c> sensitive.</param>
    public delegate void AddHeaderDelegate(byte[] name, byte[] value, bool sensitive);
}
