﻿using Ding.Ui.Builders;

namespace Ding.Ui.Angular.Builders {
    /// <summary>
    /// ng-template模板生成器
    /// </summary>
    public class TemplateBuilder : TagBuilder {
        /// <summary>
        /// 初始化ng-template模板生成器
        /// </summary>
        public TemplateBuilder() : base( "ng-template" ) {
        }
    }
}
