﻿using Ding.Ui.Builders;

namespace Ding.Ui.Angular.Builders {
    /// <summary>
    /// router-outlet生成器
    /// </summary>
    public class RouterOutletBuilder : TagBuilder {
        /// <summary>
        /// 初始化router-outlet生成器
        /// </summary>
        public RouterOutletBuilder() : base( "router-outlet" ) {
        }
    }
}