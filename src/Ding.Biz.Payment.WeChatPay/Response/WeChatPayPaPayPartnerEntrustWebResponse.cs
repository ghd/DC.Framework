﻿using System.Xml.Serialization;

namespace Ding.Payment.WeChatPay.Response
{
    [XmlRoot("xml")]
    public class WeChatPayPaPayPartnerEntrustWebResponse : WeChatPayResponse
    {
    }
}
