﻿namespace Ding.Turing
{
    public class TuringRequestUserInfo
    {
        public string ApiKey { get; set; }
        public string UserId { get; set; }
    }
}
