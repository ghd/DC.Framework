﻿using System.Threading;

namespace Ding.Util
{
    /// <summary>
    /// This static class provides helper methods for working with <c>System.Threading</c>.
    /// </summary>
    /// \author Matt Bolt
    public static class ThreadHelper
    {

        /// <summary>
        /// This method returns the current <c>Thread</c> instance's <c>ManagedThreadId</c> as a 
        /// <c>string</c>.
        /// </summary>
        /// <value>The current thread's managed thread id.</value>
        public static string CurrentThreadId
        {
            get
            {
                return Thread.CurrentThread.ManagedThreadId.ToString();
            }
        }

    }
}
