﻿using System;
using System.Collections.Generic;

namespace Ding.Threading
{
    /// <summary>
    /// This class stores a static instance of <c>T</c> in each <c>Thread</c> it is
    /// accessed. It uses an internal implementation of the <c>ThreadStatic</c>
    /// attribute. 
    /// </summary>
    /// <typeparam name="T">The type local to each thread.</typeparam>
    /// \author Matt Bolt
    public class ThreadLocal<T>
    {

        // Generate a unique ThreadLocal identifier for each instance
        private static readonly AtomicInt ThreadLocalIds;

        [ThreadStatic]
        private static Dictionary<int, T> Instances;

        static ThreadLocal()
        {
            ThreadLocalIds = new AtomicInt(0);
        }

        private int _id;
        private Func<T> _factory;

        /// <summary>
        /// Creates a new <c>ThreadLocal</c> instance using a <c>Func</c> capable
        /// of creating new <c>T</c> instances.
        /// </summary>
        /// <param name="factory">
        /// The <c>Func</c> instance used to create <c>T</c> instances.
        /// </param>
        public ThreadLocal(Func<T> factory)
        {
            _id = ThreadLocalIds.Increment();
            _factory = factory;
        }

        /// <summary>
        /// This method retreives the <c>T</c> instance local to the current <c>Thread</c>. 
        /// If an instance does not exist, one is created via the <c>Func</c> passed
        /// via the constructor.
        /// </summary>
        /// <returns>
        /// The <c>T</c> instance local to the current thread.
        /// </returns>
        public T Get()
        {
            if (null == Instances)
            {
                Instances = new Dictionary<int, T>();
            }

            if (!Instances.ContainsKey(_id))
            {
                Instances[_id] = _factory();
            }

            return Instances[_id];
        }

        /// <summary>
        /// This method allows you to set the <c>T</c> instance local to the current
        /// <c>Thread</c>. 
        /// </summary>
        /// <param name="instance">
        /// The <c>T</c> instance to set on the local <c>Thread</c>.
        /// </param>
        public void Set(T instance)
        {
            if (null == Instances)
            {
                Instances = new Dictionary<int, T>();
            }

            Instances[_id] = instance;
        }
    }
}
