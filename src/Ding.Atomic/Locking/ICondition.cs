﻿namespace Ding.Locking
{
    /// <summary>
    /// This interface defines a condition instance that can be used for flow control
    /// within a lock. 
    /// 
    /// <para>
    /// For instance, two conditions can be used to created a bounded buffer where one
    /// condition waits while the buffer is empty, and the other waits when the buffer 
    /// is full. When items are removed from the buffer, the condition waiting on full
    /// will be signaled. When items are added to the buffer, the condition waiting on
    /// empty will be signaled.
    /// </para>
    /// </summary>
    public interface ICondition
    {

        /// <summary>
        /// Signals all threads waiting on this condition.
        /// </summary>
        void Signal();

        /// <summary>
        /// Blocks the current thread until the condition is signaled.
        /// </summary>
        void Await();
    }
}
