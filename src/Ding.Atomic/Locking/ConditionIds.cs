﻿namespace Ding.Locking
{
    /// <summary>
    /// This class is used to generate condition ids for lock instances. The ids generated
    /// need only be unique per each lock instance, and are *not* exposed outside of the lock.
    /// </summary>
    /// \author Matt Bolt
    public class ConditionIds
    {
        private readonly string _prefix;
        private readonly AtomicLong _ids;

        public ConditionIds(string prefix, long startingId)
        {
            _prefix = prefix;
            _ids = new AtomicLong(startingId);
        }

        /// <summary>
        /// Creates and returns the next condition id.
        /// </summary>
        /// <remarks>This method is thread safe.</remarks>
        public string Next()
        {
            return _prefix + '-' + _ids.Increment();
        }
    }
}
