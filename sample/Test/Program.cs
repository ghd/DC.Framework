﻿using Ding.Helpers;
using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var timestamp = UnixTime.ToTimestamp();

            Console.WriteLine("timestamp:" + timestamp);

            var nonce = Randoms.GetNO();

            Console.WriteLine("nonce:" + nonce);

            var str = CheckSignature.Create(timestamp, nonce, "");

            Console.WriteLine(str);

            Console.WriteLine("检验成功：" + CheckSignature.Check(str, timestamp, nonce, "", out string sig));

            Console.ReadKey();
        }
    }
}
